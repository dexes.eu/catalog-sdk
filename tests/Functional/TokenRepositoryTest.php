<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Dexes\CatalogSdk\CatalogSdk;
use Dexes\CatalogSdk\HttpRequestService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class TokenRepositoryTest extends TestCase
{
    public function testGetTokenSuccessful(): void
    {
        try {
            $id   = 1;
            $name = 'users';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/users/1/tokens')
                    ->andReturn($this->createMockedResponse(200, 'response/token-get.valid.json'));
            });

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->tokenRepository($name);

            $token = $repo->getToken($id);
            Assert::assertIsArray($token);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testGetTokenFailed(): void
    {
        try {
            $id   = 1;
            $name = 'users';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/users/1/tokens')
                    ->andReturn($this->createMockedResponse(500, 'response/empty.json'));
            });

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->tokenRepository($name);

            $repo->getToken($id);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testStoreTokenSuccessful(): void
    {
        try {
            $id   = 1;
            $name = 'applications';
            $data = [
                'name'     => 'foo',
                'token_id' => 'auth_token',
            ];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('postJson')
                    ->with('api/applications/1/tokens', $data)
                    ->andReturn($this->createMockedResponse(200, 'response/token-store.valid.json'));
            });

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->tokenRepository($name);

            $token = $repo->storeToken($id, $data);
            Assert::assertIsArray($token);
            Assert::assertArrayHasKey('type', $token);
            Assert::assertArrayHasKey('token', $token);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testStoreTokenUnsuccessful(): void
    {
        try {
            $id   = 1;
            $name = 'applications';
            $data = [];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('postJson')
                    ->with('api/applications/1/tokens', $data)
                    ->andReturn($this->createMockedResponse(500, 'response/empty.json'));
            });

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->tokenRepository($name);

            $repo->storeToken($id, $data);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDeleteTokenSuccessful(): void
    {
        try {
            $id      = 1;
            $tokenId = 2;
            $name    = 'applications';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('delete')
                    ->with('api/applications/1/tokens/2')
                    ->andReturn($this->createMockedResponse(204, 'response/empty.json'));
            });

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->tokenRepository($name);

            $response = $repo->deleteToken($id, $tokenId);
            $this->assertTrue($response);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDeleteTokenFailed(): void
    {
        try {
            $id      = 1;
            $tokenId = 2;
            $name    = 'applications';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('delete')
                    ->with('api/applications/1/tokens/2')
                    ->andReturn($this->createMockedResponse(500, 'response/empty.json'));
            });

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->tokenRepository($name);

            $this->expectException(ResponseException::class);
            $response = $repo->deleteToken($id, $tokenId);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }
}
