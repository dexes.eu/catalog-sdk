<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Dexes\CatalogSdk\HttpRequestService;
use Dexes\CatalogSdk\Repository\IShareRepository;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class IShareRepositoryTest extends TestCase
{
    public function testPartiesSuccessFull(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/ishare/parties')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/ishare-parties.valid.json'
                    ));
            });

            $repository = new IShareRepository($requestService);
            $parties    = $repository->parties(false);

            Assert::assertIsArray($parties);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPartiesUnSuccessFull(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/ishare/parties')
                    ->andReturn($this->createMockedResponse(
                        500,
                        'response/empty.json'
                    ));
            });

            $repository = new IShareRepository($requestService);
            $repository->parties(false);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testCapabilitiesSuccessFull(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/ishare/capabilities')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/ishare-capabilities.valid.json'
                    ));
            });

            $repository      = new IShareRepository($requestService);
            $capabilities    = $repository->capabilities(false);

            Assert::assertIsArray($capabilities);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testCapabilitiesUnSuccessFull(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/ishare/capabilities')
                    ->andReturn($this->createMockedResponse(
                        500,
                        'response/empty.json'
                    ));
            });

            $repository = new IShareRepository($requestService);
            $repository->capabilities(false);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }
}
