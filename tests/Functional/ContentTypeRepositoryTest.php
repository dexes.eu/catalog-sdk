<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Dexes\CatalogSdk\HttpRequestService;
use Dexes\CatalogSdk\Repository\ContentTypeRepository;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class ContentTypeRepositoryTest extends TestCase
{
    public function testGetResponse(): void
    {
        try {
            $id   = 'foo';
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/bar/foo', [])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-show.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $content    = $repository->get($id);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('id', $content);
            Assert::assertArrayHasKey('field-one', $content);
            Assert::assertArrayHasKey('field-two', $content);
            Assert::assertArrayHasKey('field-three', $content);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testGetResponseFails(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id   = 'foo';
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/bar/foo', [])
                    ->andReturn($this->createMockedREsponse(
                        400,
                        'response/content-type-show.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $repository->get($id);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testStoreSuccessful(): void
    {
        try {
            $data = [
                'key-1' => 'data',
                'key-2' => 'baz',
            ];
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('postJson')
                    ->with('api/bar', $data)
                    ->andReturn($this->createMockedResponse(
                        201,
                        'response/content-type-store.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $content    = $repository->store($data);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('key-1', $content);
            Assert::assertArrayHasKey('key-2', $content);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testStoreFailed(): void
    {
        try {
            $this->expectException(ResponseException::class);
            $data = [
                'key-1' => 'data',
                'key-2' => 'baz',
            ];
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('postJson')
                    ->with('api/bar', $data)
                    ->andReturn($this->createMockedResponse(
                        400,
                        'response/content-type-store.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $content    = $repository->store($data);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testUpdateSuccessful(): void
    {
        try {
            $data = [
                'key-1' => 'data',
                'key-2' => 'baz',
            ];
            $id   = 'foo';
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('putJson')
                    ->with('api/bar/foo', [], $data)
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-store.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $content    = $repository->update($id, $data);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('key-1', $content);
            Assert::assertArrayHasKey('key-2', $content);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testUpdateFailed(): void
    {
        try {
            $this->expectException(ResponseException::class);
            $data = [
                'key-1' => 'data',
                'key-2' => 'baz',
            ];
            $id   = 'foo';
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('putJson')
                    ->with('api/bar/foo', [], $data)
                    ->andReturn($this->createMockedResponse(
                        400,
                        'response/content-type-store.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $content    = $repository->update($id, $data);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDeleteSuccessful(): void
    {
        try {
            $id   = 'foo';
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('delete')
                    ->with('api/bar/foo')
                    ->andReturn($this->createMockedResponse(
                        204,
                        'response/empty.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);

            $this->assertTrue($repository->delete($id));
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDeleteFailed(): void
    {
        try {
            $this->expectException(ResponseException::class);
            $id   = 'foo';
            $name = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('delete')
                    ->with('api/bar/foo')
                    ->andReturn($this->createMockedResponse(
                        400,
                        'response/empty.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $repository->delete($id);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPaginationResponse(): void
    {
        try {
            $name    = 'bar';
            $start   = 0;
            $rows    = 10;
            $filters = [];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start, $rows, $filters) {
                $mock->shouldReceive('get')
                    ->with('api/bar', [
                        'start'          => $start,
                        'rows'           => $rows,
                        'identifierOnly' => false,
                        'filters'        => $filters,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-pagination-response.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $content    = $repository->paginated($start, $rows, $filters);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('meta', $content);
            $meta = $content['meta'];
            Assert::assertIsArray($meta);
            Assert::assertArrayHasKey('start', $meta);
            Assert::assertArrayHasKey('rows', $meta);
            Assert::assertArrayHasKey('total', $meta);

            Assert::assertArrayHasKey('results', $content);
            $results = $content['results'];
            Assert::assertIsArray($results);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPaginationResponseFails(): void
    {
        try {
            $name    = 'bar';
            $start   = 0;
            $rows    = 10;
            $filters = [];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start, $rows, $filters) {
                $mock->shouldReceive('get')
                    ->with('api/bar', [
                        'start'          => $start,
                        'rows'           => $rows,
                        'identifierOnly' => false,
                        'filters'        => $filters,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-pagination-response.invalid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $repository->paginated($start, $rows, $filters);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testAllResponse(): void
    {
        $start = 0;
        $rows  = 2;
        $name  = 'bar';

        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start, $rows) {
                $mock->shouldReceive('get')
                    ->with('api/bar', [
                        'start'          => $start,
                        'rows'           => $rows,
                        'identifierOnly' => false,
                        'filters'        => [],
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-all-1.valid.json'
                    ));

                $mock->shouldReceive('get')
                    ->with('api/bar', [
                        'start'          => 2,
                        'rows'           => $rows,
                        'identifierOnly' => false,
                        'filters'        => [],
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-all-2.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $content    = $repository->all($rows);

            Assert::assertIsArray($content);
            Assert::assertCount(3, $content);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testCount(): void
    {
        $start = 0;
        $name  = 'bar';

        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start) {
                $mock->shouldReceive('get')
                    ->with('api/bar', [
                        'start'          => $start,
                        'rows'           => 1000,
                        'identifierOnly' => false,
                        'filters'        => [],
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-count.valid.json'
                    ));
            });

            $repository = new ContentTypeRepository($requestService, $name);
            $count      = $repository->count();

            Assert::assertIsInt($count);
            Assert::assertEquals(3, $count);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }
}
