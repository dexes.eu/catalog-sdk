<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use Dexes\CatalogSdk\HttpRequestService;
use Dexes\CatalogSdk\Repository\ListRepository;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class ListRepositoryTest extends TestCase
{
    /**
     * Provides structure data of the lists for testing the get() method of the ListRepository class.
     *
     * @return array[] an array of arrays containing list names and their expected structures
     */
    public static function listsKeysDataProvider(): array
    {
        return [
            'organizations list fields' => [
                'organizations', [
                    'http://organization.com' => [
                        'labels',
                        'type',
                    ],
                ],
            ],
            'dataspaces list fields' => [
                'dataspaces', [
                    'http://dataspace.com' => [
                        'labels',
                    ],
                ],
            ],
            'licenses list fields' => [
                'licenses', [
                    'http://license.com' => [
                        'labels',
                    ],
                ],
            ],
            'policies list fields' => [
                'policies', [
                    'id' => [
                        'labels',
                        'id',
                        'policy_info',
                        'policy',
                        'authority',
                        'validations',
                        'publisher',
                        'foundation',
                        'issuer',
                        'execution',
                        'references',
                        'version_id',
                        'valid_until',
                        'root_id',
                        'parent_id',
                        'created',
                        'published',
                        'policy_status',
                    ],
                ],
            ],
            'ruleframeworks list fields' => [
                'ruleframeworks', [
                    'http://ruleframework.com' => [
                        'labels',
                    ],
                ],
            ],
            'catalogs list fields' => [
                'catalogs', [
                    'http://catalog.com' => [
                        'labels',
                        'slug',
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function tearDown(): void
    {
        M::close();
    }

    /**
     *  Tests the all() method of the ListRepository class.
     *
     * @throws ClientException   Thrown when the API request did not succeed for any reason
     * @throws ResponseException Thrown when the request could not be sent
     */
    public function testAllResponse(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/lists')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/lists-all.valid.json'
                    ));
            });

            $repository = new ListRepository($requestService);
            $content    = $repository->all();

            Assert::assertIsArray($content);
            Assert::assertContains('https://api.domain.com/api/lists/organizations', $content);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * Tests the all() method of the ListRepository class to ensure it fails properly.
     *
     * @throws ClientException   Thrown when the API request did not succeed for any reason
     * @throws ResponseException Thrown when the request could not be sent
     */
    public function testAllResponseFails(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/lists')
                    ->andReturn($this->createMockedResponse(
                        400,
                        'response/lists-all.valid.json'
                    ));
            });

            $repository = new ListRepository($requestService);
            $content    = $repository->all();

            Assert::assertIsArray($content);
            Assert::assertContains('https://api.domain.com/api/lists/organizations', $content);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * Tests the get() method of the ListRepository class with various list names and their expected fields.
     *
     * @dataProvider listsKeysDataProvider
     *
     * @param string $listName the name of the list to retrieve
     * @param array  $list     the expected structure of the list, with keys being the list items
     *                         and values being the expected fields for each item
     *
     * @throws ClientException   Thrown when the API request did not succeed for any reason
     * @throws ResponseException Thrown when the request could not be sent
     */
    public function testGetResponse(string $listName, array $list): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($listName) {
                $mock->shouldReceive('get')
                    ->with(sprintf('api/lists/%s', $listName))
                    ->andReturn($this->createMockedResponse(
                        200,
                        sprintf('response/lists-%s.valid.json', $listName)
                    ));
            });

            $repository = new ListRepository($requestService);
            $content    = $repository->get($listName);

            Assert::assertIsArray($content);
            foreach ($list as $listItem => $listFields) {
                foreach ($listFields as $listField) {
                    Assert::assertArrayHasKey($listField, $content[$listItem]);
                }
            }
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * Tests the get() method of the ListRepository class with various list names and their expected fields to ensure it fails properly.
     *
     * @dataProvider listsKeysDataProvider
     *
     * @param string $listName the name of the list to retrieve
     * @param array  $list     the expected structure of the list, with keys being the list items
     *                         and values being the expected fields for each item
     *
     * @throws ClientException   Thrown when the API request did not succeed for any reason
     * @throws ResponseException Thrown when the request could not be sent
     */
    public function testGetResponseFails(string $listName, array $list): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($listName) {
                $mock->shouldReceive('get')
                    ->with(sprintf('api/lists/%s', $listName))
                    ->andReturn($this->createMockedResponse(
                        400,
                        sprintf('response/lists-%s.valid.json', $listName)
                    ));
            });

            $repository = new ListRepository($requestService);
            $content    = $repository->get($listName);

            Assert::assertIsArray($content);
            foreach ($list as $listItem => $listFields) {
                foreach ($listFields as $listField) {
                    Assert::assertArrayHasKey($listField, $content[$listItem]);
                }
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }
}
