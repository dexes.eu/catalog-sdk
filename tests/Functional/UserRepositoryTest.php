<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Dexes\CatalogSdk\CatalogSdk;
use Dexes\CatalogSdk\HttpRequestService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class UserRepositoryTest extends TestCase
{
    public function testRegisterSuccessful(): void
    {
        try {
            $data = [
                'name'                  => 'foo',
                'email'                 => 'example@email.com',
                'password'              => 'bar',
                'password_confirmation' => 'bar',
            ];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('postJson')
                    ->with('api/users/register', $data)
                    ->andReturn($this->createMockedResponse(201, 'response/user-register.valid.json'));
            });

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->users();

            $response = $repo->register($data);
            Assert::assertIsArray($response);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testRegisterFailed(): void
    {
        try {
            $data = [];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('postJson')
                    ->with('api/users/register', $data)
                    ->andReturn($this->createMockedResponse(422, 'response/empty.json'));
            });

            $this->expectException(ResponseException::class);

            $sdk  = new CatalogSdk($requestService);
            $repo = $sdk->users();

            $repo->register($data);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }
}
