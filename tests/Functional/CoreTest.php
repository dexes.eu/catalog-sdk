<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Dexes\CatalogSdk\HttpRequestService;
use Dexes\CatalogSdk\Repository\CoreRepository;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class CoreTest extends TestCase
{
    public function testResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/status')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/status.valid.json'
                    ));
            });

            $repository = new CoreRepository($requestService);
            $status     = $repository->status();

            Assert::assertIsArray($status);
            Assert::assertArrayHasKey('application', $status);
            Assert::assertArrayHasKey('interface', $status);
            Assert::assertArrayHasKey('registration', $status);
            Assert::assertArrayHasKey('content-types', $status);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/status')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/empty.json'
                    ));
            });

            $repository = new CoreRepository($requestService);
            $repository->status();
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/status')
                    ->andReturn($this->createMockedResponse(
                        404,
                        'response/status.valid.json'
                    ));
            });

            $repository = new CoreRepository($requestService);
            $repository->status();
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }
}
