<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Dexes\CatalogSdk\HttpRequestService;
use Dexes\CatalogSdk\Repository\DistributionRepository;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class DistributionRepositoryTest extends TestCase
{
    public function testGetResponse(): void
    {
        try {
            $datasetId        = 'foo';
            $distributionId   = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/dcat2/datasets/foo/distributions/bar', [])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/json-api-show.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $content    = $repository->get($datasetId, $distributionId);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('jsonapi', $content);
            Assert::assertArrayHasKey('data', $content);
            $data = $content['data'];

            Assert::assertArrayHasKey('id', $data);
            Assert::assertArrayHasKey('type', $data);
            Assert::assertArrayHasKey('attributes', $data);
            Assert::assertArrayHasKey('links', $data);
            Assert::assertArrayHasKey('meta', $data);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testGetResponseFails(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $datasetId        = 'foo';
            $distributionId   = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/dcat2/datasets/foo/distributions/bar', [])
                    ->andReturn($this->createMockedREsponse(
                        400,
                        'response/json-api-show.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $repository->get($datasetId, $distributionId);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testStoreSuccessful(): void
    {
        try {
            $postData = [
                'attributes' => [
                    'fielda' => 'data',
                ],
                'meta' => [
                    'dataspace_uri' => 'https://open.dexspace.nl',
                ],
            ];
            $datasetId = 'foo';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($postData) {
                $mock->shouldReceive('postJson')
                    ->with('api/dcat2/datasets/foo/distributions', $postData)
                    ->andReturn($this->createMockedResponse(
                        201,
                        'response/json-api-store.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $content    = $repository->store($datasetId, $postData);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('jsonapi', $content);
            Assert::assertArrayHasKey('data', $content);
            $data = $content['data'];

            Assert::assertArrayHasKey('id', $data);
            Assert::assertArrayHasKey('type', $data);
            Assert::assertArrayHasKey('attributes', $data);
            $attributes = $data['attributes'];
            Assert::assertSame($attributes['fielda'], $postData['attributes']['fielda']);
            Assert::assertArrayHasKey('links', $data);
            Assert::assertArrayHasKey('meta', $data);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testStoreFailed(): void
    {
        try {
            $this->expectException(ResponseException::class);
            $putData = [
                'attributes' => [
                    'fielda' => 'data',
                ],
                'meta' => [
                    'dataspace_uri' => 'https://open.dexspace.nl',
                ],
            ];
            $datasetId = 'foo';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($putData) {
                $mock->shouldReceive('postJson')
                    ->with('api/dcat2/datasets/foo/distributions', $putData)
                    ->andReturn($this->createMockedResponse(
                        400,
                        'response/json-api-store.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $content    = $repository->store($datasetId, $putData);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('jsonapi', $content);
            Assert::assertArrayHasKey('data', $content);
            $data = $content['data'];

            Assert::assertArrayHasKey('id', $data);
            Assert::assertArrayHasKey('type', $data);
            Assert::assertArrayHasKey('attributes', $data);
            $attributes = $data['attributes'];
            Assert::assertSame($attributes['fielda'], $putData['attributes']['fielda']);
            Assert::assertArrayHasKey('links', $data);
            Assert::assertArrayHasKey('meta', $data);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testUpdateSuccessful(): void
    {
        try {
            $data = [
                'key-1' => 'data',
                'key-2' => 'baz',
            ];
            $datasetId        = 'foo';
            $distributionId   = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('patchJson')
                    ->with('api/dcat2/datasets/foo/distributions/bar', [], $data)
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/json-api-store.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $content    = $repository->update($datasetId, $distributionId, $data);

            Assert::assertIsArray($content);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testUpdateFailed(): void
    {
        try {
            $this->expectException(ResponseException::class);
            $data = [
                'key-1' => 'data',
                'key-2' => 'baz',
            ];
            $datasetId        = 'foo';
            $distributionId   = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($data) {
                $mock->shouldReceive('patchJson')
                    ->with('api/dcat2/datasets/foo/distributions/bar', [], $data)
                    ->andReturn($this->createMockedResponse(
                        400,
                        'response/content-type-store.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $content    = $repository->update($datasetId, $distributionId, $data);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDeleteSuccessful(): void
    {
        try {
            $datasetId        = 'foo';
            $distributionId   = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('delete')
                    ->with('api/dcat2/datasets/foo/distributions/bar')
                    ->andReturn($this->createMockedResponse(
                        204,
                        'response/empty.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);

            $this->assertTrue($repository->delete($datasetId, $distributionId));
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDeleteFailed(): void
    {
        try {
            $this->expectException(ResponseException::class);
            $datasetId        = 'foo';
            $distributionId   = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('delete')
                    ->with('api/dcat2/datasets/foo/distributions/bar')
                    ->andReturn($this->createMockedResponse(
                        400,
                        'response/empty.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $repository->delete($datasetId, $distributionId);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPaginationResponse(): void
    {
        try {
            $datasetId = 'foo';
            $start     = 0;
            $rows      = 10;
            $filters   = [];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start, $rows, $filters) {
                $mock->shouldReceive('get')
                    ->with('api/dcat2/datasets/foo/distributions', [
                        'start'            => $start,
                        'rows'             => $rows,
                        'filters'          => $filters,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/json-api-pagination-response.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $content    = $repository->paginated($datasetId, $start, $rows, $filters);

            Assert::assertIsArray($content);
            Assert::assertArrayHasKey('meta', $content);
            $meta = $content['meta'];
            Assert::assertIsArray($meta);
            Assert::assertArrayHasKey('start', $meta);
            Assert::assertArrayHasKey('rows', $meta);
            Assert::assertArrayHasKey('total', $meta);

            Assert::assertArrayHasKey('data', $content);
            $results = $content['data'];
            Assert::assertIsArray($results);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPaginationResponseFails(): void
    {
        try {
            $datasetId = 'foo';
            $start     = 0;
            $rows      = 10;
            $filters   = [];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start, $rows, $filters) {
                $mock->shouldReceive('get')
                    ->with('api/dcat2/datasets/foo/distributions', [
                        'start'            => $start,
                        'rows'             => $rows,
                        'filters'          => $filters,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/content-type-pagination-response.invalid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $repository->paginated($datasetId, $start, $rows, $filters);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testAllResponse(): void
    {
        $start            = 0;
        $rows             = 2;
        $datasetId        = 'foo';
        $distributionId   = 'bar';

        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start, $rows) {
                $mock->shouldReceive('get')
                    ->with('api/dcat2/datasets/foo/distributions', [
                        'start'            => $start,
                        'rows'             => $rows,
                        'filters'          => [],
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/json-api-all-1.valid.json'
                    ));

                $mock->shouldReceive('get')
                    ->with('api/dcat2/datasets/foo/distributions', [
                        'start'            => 2,
                        'rows'             => $rows,
                        'filters'          => [],
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/json-api-all-2.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $content    = $repository->all($datasetId, $rows);

            Assert::assertIsArray($content);
            Assert::assertCount(3, $content);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testCount(): void
    {
        $start            = 0;
        $datasetId        = 'foo';
        $distributionId   = 'bar';

        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($start) {
                $mock->shouldReceive('get')
                    ->with('api/dcat2/datasets/foo/distributions', [
                        'start'            => $start,
                        'rows'             => 1000,
                        'filters'          => [],
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'response/json-api-count.valid.json'
                    ));
            });

            $repository = new DistributionRepository($requestService);
            $count      = $repository->count($datasetId);

            Assert::assertIsInt($count);
            Assert::assertEquals(3, $count);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }
}
