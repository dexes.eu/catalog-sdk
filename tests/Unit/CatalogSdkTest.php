<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Dexes\CatalogSdk\CatalogSdk;
use Dexes\CatalogSdk\HttpRequestService;
use Mockery as M;
use Tests\TestCase;

/**
 * @internal
 */
class CatalogSdkTest extends TestCase
{
    public static function repositoryNames(): array
    {
        return [
            ['core'],
            ['distributions'],
            ['users'],
            ['IShare'],
            ['lists'],
        ];
    }

    public static function contentTypeRepositoryNames(): array
    {
        return [
            ['dataspace'],
            ['ruleframework'],
            ['applications'],
        ];
    }

    public static function jsonApiRepositoryNames(): array
    {
        return [
            ['dcat2/datasets'],
            ['dcat2/dataservices'],
        ];
    }

    public static function tokenRepositoryNames(): array
    {
        return [
            ['users'],
            ['applications'],
        ];
    }

    public static function tokenRepositoryNamesWithSdkFunction(): array
    {
        return [
            ['users', 'userTokens'],
            ['applications', 'applicationTokens'],
        ];
    }

    public static function jsonApiRepositoryNamesWithSdkFunction(): array
    {
        return [
            ['dcat2/datasets', 'datasets'],
            ['dcat2/dataservices', 'dataservices'],
        ];
    }

    public static function contentTypeRepositoryNamesWithSdkFunction(): array
    {
        return [
            ['applications', 'applications'],
        ];
    }

    /**
     * @dataProvider repositoryNames
     */
    public function testSameRepositoryIsReturnedEveryTime(string $repositoryName): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CatalogSdk($requestService);

        $this->assertSame($sdk->{$repositoryName}(), $sdk->{$repositoryName}());
    }

    /**
     * @dataProvider contentTypeRepositoryNames
     */
    public function testSameContentTypeRepositoryIsReturnedEveryTime(string $contentType): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CatalogSdk($requestService);

        $this->assertSame($sdk->contentTypeRepository($contentType), $sdk->contentTypeRepository($contentType));
    }

    /**
     * @dataProvider jsonApiRepositoryNames
     */
    public function testSameJsonApiRepositoryIsReturnedEveryTime(string $name): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CatalogSdk($requestService);

        $this->assertSame($sdk->jsonApiRepository($name), $sdk->jsonApiRepository($name));
    }

    /**
     * @dataProvider jsonApiRepositoryNamesWithSdkFunction
     */
    public function testDatasetRepository(string $name, string $function): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CatalogSdk($requestService);

        $this->assertSame($sdk->jsonApiRepository($name), $sdk->{$function}());
    }

    /**
     * @dataProvider tokenRepositoryNames
     */
    public function testSameTokenRepositoryIsReturnedEveryTime(string $name): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CatalogSdk($requestService);

        $this->assertSame($sdk->tokenRepository($name), $sdk->tokenRepository($name));
    }

    /**
     * @dataProvider tokenRepositoryNamesWithSdkFunction
     */
    public function testTokenRepository(string $name, string $function): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CatalogSdk($requestService);

        $this->assertSame($sdk->tokenRepository($name), $sdk->{$function}());
    }

    /**
     * @dataProvider contentTypeRepositoryNamesWithSdkFunction
     */
    public function testContentTypeRepositoriesFunctions(string $name, string $function): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CatalogSdk($requestService);

        $this->assertSame($sdk->contentTypeRepository($name), $sdk->{$function}());
    }
}
