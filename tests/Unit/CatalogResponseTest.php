<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Dexes\CatalogSdk\CatalogResponse;
use Mockery as M;
use PHPUnit\Framework\Assert;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @internal
 */
class CatalogResponseTest extends TestCase
{
    public function testHasValidJson(): void
    {
        $status   = 200;
        $fixture  = 'response/status.valid.json';

        $catalogResponse = $this->createMockedResponse($status, $fixture);

        Assert::assertTrue($catalogResponse->hasStatus($status));
        Assert::assertTrue($catalogResponse->hasJson());
        Assert::assertTrue($catalogResponse->hasValidJson('status.json'));
        Assert::assertEquals(json_decode($this->loadFixture($fixture)), $catalogResponse->json());
    }

    public function testHasInvalidJsonAccordingToSchema(): void
    {
        $status  = 200;
        $fixture = 'response/empty.json';

        $ckanResponse = $this->createMockedResponse($status, $fixture);

        Assert::assertTrue($ckanResponse->hasStatus($status));
        Assert::assertTrue($ckanResponse->hasJson());
        Assert::assertFalse($ckanResponse->hasValidJson('status.json'));
        Assert::assertEquals(json_decode($this->loadFixture($fixture)), $ckanResponse->json());
    }

    public function testOriginalResponse(): void
    {
        $psrResponse  = M::mock(ResponseInterface::class);
        $ckanResponse = new CatalogResponse($psrResponse);

        Assert::assertSame($ckanResponse->getPsrResponse(), $psrResponse);
    }
}
