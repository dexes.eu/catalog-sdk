# Dexes / Catalog SDK

[gitlab.com/dexes.eu/catalog-sdk](https://gitlab.com/dexes.eu/catalog-sdk)

Dexes package providing a SDK for interacting with the [Catalog API](https://api.dexes.eu).

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of ... 

```shell
composer require dexes/catalog-sdk
```
