<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk;

use XpertSelect\PsrTools\PsrResponse;

/**
 * Class CatalogResponse.
 */
final class CatalogResponse extends PsrResponse
{
    /**
     * {@inheritdoc}
     */
    public function getJsonSchemaPath(): string
    {
        return __DIR__ . '/../resources/json-schemas/';
    }
}
