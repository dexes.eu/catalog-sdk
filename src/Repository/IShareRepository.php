<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk\Repository;

use Dexes\CatalogSdk\HttpRequestService;
use Dexes\CatalogSdk\JWT;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class IShareRepository.
 */
class IShareRepository
{
    /**
     * IShareRepository Constructor.
     *
     * @param HttpRequestService $requestService The service for interacting with the HTTP API
     */
    public function __construct(protected HttpRequestService $requestService)
    {
    }

    /**
     * Execute a GET request to the /api/ishare/parties endpoint.
     *
     * @param bool $decoded Whether the response should be decoded
     *
     * @return array<int, mixed> A list with all parties
     *
     * @throws ClientException   thrown when the request could not be sent
     * @throws ResponseException thrown when the API request did not succeed for any reason
     */
    public function parties(bool $decoded): array
    {
        $response = $this->requestService->get('api/ishare/parties');

        if ($response->hasStatus(200)) {
            $json = $response->json(true);

            return $decoded ? JWT::decode($json['parties_token']) : $json;
        }

        throw new ResponseException($response);
    }

    /**
     * Execute a GET request to the /api/ishare/parties/{eori} endpoint.
     *
     * @param string $eori The EORI of the iSHARE party
     *
     * @return array<string, mixed> The iSHARE party info
     *
     * @throws ClientException   thrown when the request could not be sent
     * @throws ResponseException thrown when the API request did not succeed for any reason
     */
    public function party(string $eori): array
    {
        $response = $this->requestService->get('api/ishare/parties/' . $eori);

        if ($response->hasStatus(200)) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Execute a GET request to the /api/ishare/capabilities endpoint.
     *
     * @param bool $decoded Whether the response should be decoded
     *
     * @return array<int|string, mixed> The response
     *
     * @throws ClientException
     * @throws ResponseException
     */
    public function capabilities(bool $decoded): array
    {
        $response = $this->requestService->get('api/ishare/capabilities');

        if ($response->hasStatus(200)) {
            $json = $response->json(true);

            return $decoded ? JWT::decode($json['capabilities_token']) : $json;
        }

        throw new ResponseException($response);
    }
}
