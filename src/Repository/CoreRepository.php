<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk\Repository;

use Dexes\CatalogSdk\HttpRequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class CoreRepository.
 */
class CoreRepository
{
    /**
     * CoreRepository Constructor.
     *
     * @param HttpRequestService $requestService The service for interacting with the HTTP API
     */
    public function __construct(protected readonly HttpRequestService $requestService)
    {
    }

    /**
     * Request the status of the Dexes API by performing a `api/status` call.
     *
     * @return array{
     *     application: string,
     *     interface: string,
     *     registration: string,
     *     content-types: array<int, string>
     * } The status of the Dexes API
     *
     * @throws ResponseException thrown when the API request did not succeed for any reason
     * @throws ClientException   thrown when the request could not be sent
     */
    public function status(): array
    {
        $response = $this->requestService->get('api/status');

        if ($response->hasStatus(200) && $response->hasValidJson('status.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }
}
