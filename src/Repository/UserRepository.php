<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk\Repository;

use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class UserRepository.
 */
class UserRepository extends ContentTypeRepository
{
    /**
     * Send a POST request to the Catalog API to register a new user.
     *
     * @param array<string, mixed> $data The data sent to the Catalog api
     *
     * @return array<string, mixed> the response of the Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function register(array $data): array
    {
        $response = $this->requestService->postJson($this->registerPath(), jsonData: $data);

        if ($response->hasStatus(201)) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Return the path of the register api endpoint.
     */
    private function registerPath(): string
    {
        return $this->getPath() . '/register';
    }
}
