<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk\Repository;

use Dexes\CatalogSdk\HttpRequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class ListRepository.
 */
class ListRepository
{
    /**
     * ListRepository Constructor.
     *
     * @param HttpRequestService $requestService The service for interacting with the HTTP API
     */
    public function __construct(protected HttpRequestService $requestService)
    {
    }

    /**
     * Sends a GET request to the Catalog API lists endpoint to retrieve a specific list.
     *
     * @param string $listName The name of the list
     *
     * @return array<string, mixed> the response of the Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function get(string $listName): array
    {
        $response = $this->requestService->get($this->getPath($listName));

        if ($response->hasStatus(200) && $response->hasJson()) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Get all lists from the Catalog API.
     *
     * @return array<string> The result
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function all(): array
    {
        $response = $this->requestService->get($this->getPath());

        if ($response->hasStatus(200) && $response->hasJson()) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Get the path.
     *
     * @param null|string $listName The name of the list
     *
     * @return string The path
     */
    protected function getPath(?string $listName = null): string
    {
        $result = 'api/lists';

        if (!empty($listName)) {
            $result .= '/' . $listName;
        }

        return $result;
    }
}
