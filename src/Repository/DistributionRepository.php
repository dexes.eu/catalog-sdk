<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk\Repository;

use Dexes\CatalogSdk\HttpRequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class DistributionRepository.
 */
class DistributionRepository
{
    /**
     * ContentTypeRepository Constructor.
     *
     * @param HttpRequestService $requestService The service for interacting with the HTTP API
     */
    public function __construct(protected HttpRequestService $requestService)
    {
    }

    /**
     * Sends a POST request to create a new distribution on the Catalog API with the given data.
     *
     * @param string               $dataset The unique identifier of the dataset
     * @param array<string, mixed> $data    The data sent to the Catalog API
     *
     * @return array<string, mixed> The response of the Catalog API
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function store(string $dataset, array $data): array
    {
        $response = $this->requestService->postJson($this->getPath($dataset), jsonData: $data);

        if ($response->hasStatus(201) && $response->hasValidJson('json-api/baseResponse.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a PUT request to update a distribution.
     *
     * @param string               $dataset The unique identifier of the dataset
     * @param string               $id      the unique identifier of the resource
     * @param array<string, mixed> $data    the data sent to the Catalog API
     *
     * @return array<string, mixed> The response of the Catalog API
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function update(string $dataset, string $id, array $data): array
    {
        $response = $this->requestService->patchJson($this->getPath($dataset, $id), jsonData: $data);

        if ($response->hasStatus(200) && $response->hasValidJson('json-api/baseResponse.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a DELETE request to delete a distribution.
     *
     * @param string $dataset The unique identifier of the dataset
     * @param string $id      the unique identifier of the resource
     *
     * @return bool Whether the content was successfully deleted
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function delete(string $dataset, string $id): bool
    {
        $response = $this->requestService->delete($this->getPath($dataset, $id));

        if ($response->hasStatus(204)) {
            return true;
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a GET request to the Catalog API to retrieve a specific distribution.
     *
     * @param string               $dataset The unique identifier of the dataset
     * @param string               $id      the unique identifier of the resource
     * @param array<string, mixed> $params  optional query parameters
     *
     * @return array<string, mixed> the response of the Catalog API
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function get(string $dataset, string $id, array $params = []): array
    {
        $response = $this->requestService->get($this->getPath($dataset, $id), $params);

        if ($response->hasStatus(200) && $response->hasValidJson('json-api/baseResponse.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Get all distributions off a dataset from the Catalog API.
     *
     * @param string             $dataset The unique identifier of the dataset
     * @param int                $rows    The amount of rows for each request
     * @param array<int, string> $filters Any {key}:{value} filters to apply to the query
     *
     * @return array<int, array<string, mixed>> The result
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function all(string $dataset, int $rows = 1000, array $filters = []): array
    {
        $all       = [];
        $harvested = 0;

        do {
            $response = $this->paginated($dataset, $harvested, $rows, $filters);

            $all          = array_merge($all, $response['data']);
            $harvested    = count($all);
            $total        = $response['meta']['total'];
        } while ($harvested < $total);

        return $all;
    }

    /**
     * Send a GET request to the Catalog API and get a paginated response.
     *
     * @param string             $dataset The unique identifier of the dataset
     * @param int                $start   Describes from which offset to start returning records
     * @param int                $rows    The amount of rows to return
     * @param array<int, string> $filters any {key}:{value} filters to apply to the query
     *
     * @return array{
     *      jsonapi: array{
     *          "version": string,
     *      },
     *      meta: array{
     *          start: int,
     *          rows: int,
     *          total: int,
     *      },
     *      data: array<int, array<string, mixed>>
     *  } The response of the Catalog API
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function paginated(string $dataset, int $start = 0, int $rows = 1000, array $filters = []): array
    {
        $response = $this->requestService->get($this->getPath($dataset), [
            'start'     => $start,
            'rows'      => $rows,
            'filters'   => $filters,
        ]);

        if ($response->hasStatus(200) && $response->hasValidJson('json-api/paginationResponse.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Return the amount of distribution for the given dataset.
     *
     * @param string             $dataset The unique identifier of the dataset
     * @param array<int, string> $filters any {key}:{value} filters to apply to the query
     *
     * @return int The amount of distributions found
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function count(string $dataset, array $filters = []): int
    {
        return count($this->all($dataset, filters: $filters));
    }

    /**
     * Get the path.
     *
     * @param string      $dataset The unique identifier of the dataset
     * @param null|string $id      The unique identifier of the distribution
     *
     * @return string The path
     */
    private function getPath(string $dataset, ?string $id = null): string
    {
        $result = 'api/dcat2/datasets/' . $dataset . '/distributions';
        if (!empty($id)) {
            $result .= '/' . $id;
        }

        return $result;
    }
}
