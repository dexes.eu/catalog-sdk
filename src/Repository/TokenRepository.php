<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk\Repository;

use Dexes\CatalogSdk\HttpRequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class TokenRepository.
 */
class TokenRepository
{
    /**
     * TokenRepository Constructor.
     *
     * @param HttpRequestService $requestService The http request service
     * @param string             $name           The name of the token repository
     */
    public function __construct(private readonly HttpRequestService $requestService,
                                private readonly string $name)
    {
    }

    /**
     * Sends a GET request to get a token for a user or application.
     *
     * @param string $id The unique identifier of the user or application
     *
     * @return array<string, mixed> The response of hte Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function getToken(string $id): array
    {
        $response = $this->requestService->get($this->getPath($id));

        if ($response->hasStatus(200)) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a POST request to request a new token for a user or application.
     *
     * @param string               $id   The unique identifier of the user or application
     * @param array<string, mixed> $data The data send to the catalog api
     *
     * @return array<string, mixed> The response of hte Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function storeToken(string $id, array $data): array
    {
        $response = $this->requestService->postJson($this->getPath($id), jsonData: $data);

        if ($response->hasStatus(200)) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a DELETE request to delete a token from the Catalog API.
     *
     * @param string $id      The unique identifier of the user or application
     * @param string $tokenId The unique identifier of the token
     *
     * @return bool Whether the token was successfully deleted
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function deleteToken(string $id, string $tokenId): bool
    {
        $response = $this->requestService->delete($this->getPath($id, $tokenId));

        if ($response->hasStatus(204)) {
            return true;
        }

        throw new ResponseException($response);
    }

    /**
     * Get the path for.
     *
     * @param string      $id      The unique identifier of the user/application
     * @param null|string $tokenId The unique identifier of the token
     *
     * @return string The generated path
     */
    private function getPath(string $id, ?string $tokenId = null): string
    {
        $path = sprintf('api/%s/%s/tokens', $this->name, $id);

        if (!is_null($tokenId)) {
            $path .= '/' . $tokenId;
        }

        return $path;
    }
}
