<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk\Repository;

use Dexes\CatalogSdk\HttpRequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class ContentTypeRepository.
 */
class ContentTypeRepository
{
    /**
     * ContentTypeRepository Constructor.
     *
     * @param HttpRequestService $requestService The service for interacting with the HTTP API
     * @param string             $name           The name of the content-type
     */
    public function __construct(protected HttpRequestService $requestService,
                                private readonly string $name)
    {
    }

    /**
     * Sends a POST request to create a new resource on the Catalog API with the given data.
     *
     * @param array<string, mixed> $data The data sent to the Catalog API
     *
     * @return array<string, mixed> The response of the Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function store(array $data): array
    {
        $response = $this->requestService->postJson($this->getPath(), jsonData: $data);

        if ($response->hasStatus(201)) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a PUT request to update a resource.
     *
     * @param string               $id   the unique identifier of the resource
     * @param array<string, mixed> $data the data sent to the Catalog API
     *
     * @return array<string, mixed> The response of the Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function update(string $id, array $data): array
    {
        $response = $this->requestService->putJson($this->getPath($id), jsonData: $data);

        if ($response->hasStatus(200)) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a DELETE request to delete a resource.
     *
     * @param string $id the unique identifier of the resource
     *
     * @return bool Whether the content was successfully deleted
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function delete(string $id): bool
    {
        $response = $this->requestService->delete($this->getPath($id));

        if ($response->hasStatus(204)) {
            return true;
        }

        throw new ResponseException($response);
    }

    /**
     * Sends a GET request to the Catalog API to retrieve a specific resource.
     *
     * @param string               $id     the unique identifier of the resource
     * @param array<string, mixed> $params optional query parameters
     *
     * @return array<string, mixed> the response of the Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function get(string $id, array $params = []): array
    {
        $response = $this->requestService->get($this->getPath($id), $params);

        if ($response->hasStatus(200)) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Get all resources from the Catalog API.
     *
     * @param int                $rows           The amount of rows for each request
     * @param array<int, string> $filters        Any {key}:{value} filters to apply to the query
     * @param bool               $identifierOnly Whether only the identifier of the object should be returned
     *
     * @return array<int, array<string, mixed>> The result
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function all(int $rows = 1000, array $filters = [], bool $identifierOnly = false): array
    {
        $all       = [];
        $harvested = 0;

        do {
            $response = $this->paginated($harvested, $rows, $filters, $identifierOnly);

            $all          = array_merge($all, $response['results']);
            $harvested    = count($all);
            $total        = $response['meta']['total'];
        } while ($harvested < $total);

        return $all;
    }

    /**
     * Send a GET request to the Catalog API and get a paginated response.
     *
     * @param int                $start          Describes from which offset to start returning records
     * @param int                $rows           The amount of rows to return
     * @param array<int, string> $filters        any {key}:{value} filters to apply to the query
     * @param bool               $identifierOnly Whether only the identifier of the object should be returned
     *
     * @return array{
     *     meta: array{
     *         start: int,
     *         rows: int,
     *         total: int,
     *     },
     *     results: array<int, array<string, mixed>>
     * } The response of the Catalog API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function paginated(int $start = 0, int $rows = 1000, array $filters = [], bool $identifierOnly = false): array
    {
        $response = $this->requestService->get($this->getPath(), [
            'start'          => $start,
            'rows'           => $rows,
            'identifierOnly' => $identifierOnly,
            'filters'        => $filters,
        ]);

        if ($response->hasStatus(200) && $response->hasValidJson('paginationResponse.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Return the amount of records.
     *
     * @param array<int, string> $filters any {key}:{value} filters to apply to the query
     *
     * @return int The amount of records found
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function count(array $filters = []): int
    {
        return count($this->all(filters: $filters));
    }

    /**
     * Get the path.
     *
     * @param null|string $id The unique identifier of a resource
     *
     * @return string The path
     */
    protected function getPath(?string $id = null): string
    {
        $result = 'api/' . $this->name;
        if (!empty($id)) {
            $result .= '/' . $id;
        }

        return $result;
    }
}
