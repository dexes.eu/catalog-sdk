<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk;

use Psr\Http\Message\ResponseInterface;
use XpertSelect\PsrTools\HttpRequestService as HRS;
use XpertSelect\PsrTools\PsrResponse;

/**
 * Class HttpRequestService.
 *
 * A service for interacting with the Dexes HTTP API using a given PSR compliant implementation.
 */
final class HttpRequestService extends HRS
{
    /**
     * {@inheritdoc}
     */
    public function createUserAgent(): string
    {
        return 'dexes/catalog-sdk';
    }

    /**
     * {@inheritdoc}
     */
    public function getPsrResponse(ResponseInterface $response): PsrResponse
    {
        return new CatalogResponse($response);
    }
}
