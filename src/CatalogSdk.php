<?php

/**
 * This file is part of the dexes/catalog-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\CatalogSdk;

use Dexes\CatalogSdk\Repository\ContentTypeRepository;
use Dexes\CatalogSdk\Repository\CoreRepository;
use Dexes\CatalogSdk\Repository\DistributionRepository;
use Dexes\CatalogSdk\Repository\IShareRepository;
use Dexes\CatalogSdk\Repository\JsonApiRepository;
use Dexes\CatalogSdk\Repository\ListRepository;
use Dexes\CatalogSdk\Repository\TokenRepository;
use Dexes\CatalogSdk\Repository\UserRepository;
use Psr\EventDispatcher\EventDispatcherInterface;

/**
 * Class CatalogSDK.
 *
 * Sdk for interacting with the Dexes Catalog HTTP API.
 */
class CatalogSdk
{
    /**
     * The repository for interacting with the basic API endpoints of the Catalog API.
     */
    protected ?CoreRepository $coreRepository;

    /**
     * The repository for interacting with the distribution endpoints.
     */
    protected ?DistributionRepository $distributionRepository;

    /**
     * The repository for interacting with the user endpoints.
     */
    protected ?UserRepository $userRepository;

    /**
     * The repository for interacting with the iShare endpoints.
     */
    protected ?IShareRepository $IShareRepository;

    /**
     * The repository for interacting with the list endpoints.
     */
    protected ?ListRepository $listRepository;

    /**
     * The repositories for interacting with the various content type repositories.
     *
     * @var array<string, mixed>
     */
    protected array $contentTypeRepositories;

    /**
     * The repositories for interacting with the various json api repositories.
     *
     * @var array<string, mixed>
     */
    protected array $jsonApiRepositories;

    /**
     * The repositories for interacting with the auth token endpoints.
     *
     * @var array<string, mixed>
     */
    private array $tokenRepositories;

    /**
     * CatalogSdk Constructor.
     *
     * @param HttpRequestService            $requestService  The service for interacting with the HTTP API
     * @param null|EventDispatcherInterface $eventDispatcher The service for dispatching events
     */
    public function __construct(protected HttpRequestService $requestService,
                                protected ?EventDispatcherInterface $eventDispatcher = null)
    {
        $this->coreRepository          = null;
        $this->contentTypeRepositories = [];
        $this->jsonApiRepositories     = [];
        $this->tokenRepositories       = [];
        $this->distributionRepository  = null;
        $this->userRepository          = null;
        $this->IShareRepository        = null;
        $this->listRepository          = null;
    }

    /**
     * Retrieve the repository for the given content-type of the Catalog API. The repository is
     * instantiated on the first request.
     *
     * Custom implementations of the ContentTypeRepository can be `injected` by providing a custom
     * implementation of the `makeContentTypeRepository` method.
     *
     * @param string $name The name of the content type
     *
     * @return ContentTypeRepository The repository
     */
    public function contentTypeRepository(string $name): ContentTypeRepository
    {
        if (!array_key_exists($name, $this->contentTypeRepositories)
            || is_null($this->contentTypeRepositories[$name])) {
            $this->contentTypeRepositories[$name] = $this->makeContentTypeRepository($name);
        }

        return $this->contentTypeRepositories[$name];
    }

    /**
     * Retrieve the repository for the given json-api endpoints of the Catalog API. The repository
     * is instantiated on the first request.
     *
     * Custom implementations of the JsonApiRepository can be `injected` by providing a custom
     * implementation of the `makeJsonApiRepository`method.
     *
     * @param string $name The name of the json-api endpoints
     *
     * @return JsonApiRepository The repository
     */
    public function jsonApiRepository(string $name): JsonApiRepository
    {
        if (!array_key_exists($name, $this->jsonApiRepositories)
            || is_null($this->jsonApiRepositories[$name])) {
            $this->jsonApiRepositories[$name] = $this->makeJsonApiRepository($name);
        }

        return $this->jsonApiRepositories[$name];
    }

    public function iShare(): IShareRepository
    {
        if (is_null($this->IShareRepository)) {
            $this->IShareRepository = $this->makeIShareRepository();
        }

        return $this->IShareRepository;
    }

    public function lists(): ListRepository
    {
        if (is_null($this->listRepository)) {
            $this->listRepository = $this->makelistRepository();
        }

        return $this->listRepository;
    }

    public function tokenRepository(string $name): TokenRepository
    {
        if (!array_key_exists($name, $this->tokenRepositories)
            || is_null($this->tokenRepositories[$name])) {
            $this->tokenRepositories[$name] = $this->makeTokenRepository($name);
        }

        return $this->tokenRepositories[$name];
    }

    public function datasets(): JsonApiRepository
    {
        return $this->jsonApiRepository('dcat2/datasets');
    }

    public function dataservices(): JsonApiRepository
    {
        return $this->jsonApiRepository('dcat2/dataservices');
    }

    public function applications(): ContentTypeRepository
    {
        return $this->contentTypeRepository('applications');
    }

    public function distributions(): DistributionRepository
    {
        if (is_null($this->distributionRepository)) {
            $this->distributionRepository = $this->makeDistributionRepository();
        }

        return $this->distributionRepository;
    }

    public function users(): UserRepository
    {
        if (is_null($this->userRepository)) {
            $this->userRepository = $this->makeUserRepository();
        }

        return $this->userRepository;
    }

    public function userTokens(): TokenRepository
    {
        return $this->tokenRepository('users');
    }

    public function applicationTokens(): TokenRepository
    {
        return $this->tokenRepository('applications');
    }

    /**
     * Retrieve the repository for interacting with the basic endpoints of the Catalog API. The
     * repository is instantiated on the first request.
     *
     * Custom implementations of the CoreRepository can be 'injected'  by providing a custom
     * implementation of the `makeCoreRepository`  method.
     *
     * @return CoreRepository the repository for interacting with the basic endpoints
     *
     * @see CatalogSdk::makeCoreRepository()
     */
    public function core(): CoreRepository
    {
        if (is_null($this->coreRepository)) {
            $this->coreRepository = $this->makeCoreRepository();
        }

        return $this->coreRepository;
    }

    /**
     * Creates a CoreRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @return CoreRepository The created instance
     */
    protected function makeCoreRepository(): CoreRepository
    {
        return new CoreRepository($this->requestService);
    }

    /**
     * Creates a DistributionRepository implementation. Extending classes may opt to provide their
     * own implementation as a way to inject custom functionality.
     *
     * @return DistributionRepository The created instance
     */
    protected function makeDistributionRepository(): DistributionRepository
    {
        return new DistributionRepository($this->requestService);
    }

    /**
     * Creates a ContentTypeRepository implementation. Extending classes may opt to provide their
     * own implementation as a way to inject custom functionality.
     *
     * @param string $name The name of the content type
     *
     * @return ContentTypeRepository The created instance
     */
    protected function makeContentTypeRepository(string $name): ContentTypeRepository
    {
        return new ContentTypeRepository($this->requestService, $name);
    }

    /**
     * Creates a JsonApiRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @param string $name The name of the content type
     *
     * @return JsonApiRepository The created instance
     */
    protected function makeJsonApiRepository(string $name): JsonApiRepository
    {
        return new JsonApiRepository($this->requestService, $name);
    }

    /**
     * Creates a UserRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @return UserRepository The created instance
     */
    protected function makeUserRepository(): UserRepository
    {
        return new UserRepository($this->requestService, 'users');
    }

    /**
     * Creates a TokenRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @param string $name the name of the token type
     *
     * @return TokenRepository The created instance
     */
    protected function makeTokenRepository(string $name): TokenRepository
    {
        return new TokenRepository($this->requestService, $name);
    }

    /**
     * Creates a IShareRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @return IShareRepository The created instance
     */
    protected function makeIShareRepository(): IShareRepository
    {
        return new IShareRepository($this->requestService);
    }

    /**
     * Creates a ListRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @return ListRepository The created instance
     */
    protected function makeListRepository(): ListRepository
    {
        return new ListRepository($this->requestService);
    }
}
